---
layout: post
title: Hello World Wide Web
date: 2019-09-04 10:07:00 -0400
type: text
weight: 3
---
I just joined Neocities :) I'm so excited to have a place on the web to call
my own >:) Hopefully this site will be a good home for stuff I make and enjoy.
I have already posted some pictures but they are dated before this post because
I used them to work on my theme.
