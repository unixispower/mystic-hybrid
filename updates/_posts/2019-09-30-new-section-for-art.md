---
layout: post
title: New Section for Art
date: 2019-10-01 09:05:00 -0400
type: text
weight: 2
---
New section day! I added the [Art page]({{ site.baseurl }}/art/) to showcase
digital art like animated gifs and photo manipulations. The
[first post]({% post_url /art/2019-09-30-master-meowgi %}) there is of our cat
Mr. Meowgi posing with a rose. He is very photogenic, so expect more content
showcasing his beauty. Also, happy spooky month!
