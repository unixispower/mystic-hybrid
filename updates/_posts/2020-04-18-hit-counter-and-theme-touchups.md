---
layout: post
title: Hit Counter and Theme Touchups
date: 2020-04-18 08:58:00 -0400
type: text
weight: 1
---
I'm feeling quite motivated with the change in season and I've been using the
energy to spruce up things a bit around my sites. I added a hit counter that
uses the Neocities API (thanks to [Dannarchy](https://dannarchy.com/) for a
[great turorial](https://dannarchy.com/tut/tut_002.html)) and a new icon set
for the top navigation. I'm also working on some new content to post very soon.
