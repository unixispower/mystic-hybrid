---
layout: page
title: Thank You for Stopping By
no_title: true
---
Welcome to my art and photography website! This is where I post pictures I have
taken and digital art I have created. Currently I shoot pictures with a
[Sony Mavica MVC-FD71](http://camera-wiki.org/wiki/Sony_Mavica_FD71). I
love the Mavica's combination of easily-overloaded CCD sensor and noticeable
JPEG compression artifacts. You can see some of the photos I've taken with it
on the [Photos page]({{ site.baseurl }}/photos/). You can see composite stills
and GIFs I've made on the [Art page]({{ site.baseurl }}/art/). Below is one of
my recent works :)

{% include premier.html
    post_id="/photos/beans-and-pumpkin" accent="pumpkin.gif" %}

This site is constantly under construction. Please be sure to check
back often to catch new content and theme changes. Also tell your friends about
this place; they will like it too! Thanks again for stopping by and have a
wonderful day :)
