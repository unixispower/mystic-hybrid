# Website of Mystic Hybrid
Art and photography [website](https://mystichybrid.info/) built using
[Jekyll](https://jekyllrb.com/) and hosted on
[Neocities](https://neocities.org/).


## Testing
You will need to [install Jekyll](https://jekyllrb.com/docs/installation/)
before you can serve the site on your local machine. Run the following to
start a local server:

```shell
$ jekyll serve
```

Open a web browser to `localhost:8000` to access the site.


## Scripts
Scripts for graphics conversion and site upload are included in the `_utils`
directory. Specific requirements and script descriptions are found within the
header of each script.


## Licensing
Thematic markup, style sheets, and scripts in this repository are licensed under
the 2-clause BSD license, see `LICENSE` for details. Page content is excluded
from this license, see the Content section below.


### Fonts
Fonts are licensed under various licenses, see
`static/{font-name}-license.txt`.

### Graphics
Some images in the `static` dir are taken/derived from various dumps and
archives of the "old" internet. special thanks goes to:

 - [GifCities](https://gifcities.org/)
 - [CD.TEXTFILES.COM](http://cd.textfiles.com/directory.html)

If you are the owner of any of these images and have an
issue with their usage please [contact me](mailto:trash@mystichybrid.info).

### Content
All other content in this repository should be considered site content and is
copyright Blaine Murphy. This includes markdown files and the media included by
those files.

If you like any photos or art and wish to use it, please
[contact me](mailto:trash@mystichybrid.info).
